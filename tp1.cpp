#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdlib.h>
#include <unistd.h>
#include <vector>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <cmath>
#include <string.h>

using namespace std;
using namespace cv;

const char *dirExamples = "coil-100";
const char *imageExtension = ".png";
const char *dirHistogram = "histogram";

vector<string> getExamples(const char *dir);
int isImageFile(char *f);
void getHistogram(char *file, float *histogrameArray);
float calculateDistanceHistogram(float *his1, float *his2);
void knnProcessing(char *nameImgQuery);

typedef struct {
    string name;
    float distance;
}desImg;

int scale, totalPixel = 0, nbNeighbour;
float *histogramImage;
desImg *desImgs;

int main(int argc, char ** argv){
    scale = atoi(argv[2]);
    nbNeighbour = atoi(argv[3]);
    histogramImage = new float[scale * 3];
    desImgs = new desImg[nbNeighbour];

    for(int i = 0; i < nbNeighbour; i++){
        desImgs[i].name =  "unknown";
        desImgs[i].distance = 100000.0;
    }

    char imageUrl[128];
    sprintf(imageUrl, "%s/%s", dirExamples, argv[1]);
    getHistogram(imageUrl, histogramImage);

    knnProcessing(argv[1]);

    cout<<"List neighbour : \n";
    for(int i = 0; i < nbNeighbour; i++){
        cout<<desImgs[i].name<<" "<<desImgs[i].distance<<endl;
    }

    delete desImgs;
    delete histogramImage;

    return 0;
}

vector<string> getExamples(const char *dir){
    DIR *dp;
    struct dirent *ep;
    vector<string> listFile;

    dp = opendir (dir);
    if (dp != NULL){
        while (ep = readdir (dp)){
            if(isImageFile(ep->d_name)){
                listFile.push_back(ep->d_name);
            }
        }
        (void) closedir (dp);
    }else{
        perror ("Couldn't open the directory");
    }
    return listFile;
}

int isImageFile(char *f){
    if(strstr(f, imageExtension)){
        return 1;
    }
    return 0;
}

void knnProcessing(char* nameImgQuery){
    char fileImage[128];
    sprintf(fileImage, "%s/%d%s", dirHistogram, scale, imageExtension);

    vector<string> listFile;
    listFile = getExamples(dirExamples);
    for (vector<string>::iterator it = listFile.begin() ; it != listFile.end(); ++it){
        char imageUrl[128];
        if(strcmp(nameImgQuery, (*it).c_str()) != 0){
            sprintf(imageUrl, "%s/%s", dirExamples, (*it).c_str());
            float *histogramNeighbour = new float[scale * 3];
            getHistogram(imageUrl, histogramNeighbour);

            desImg tmp;
            tmp.name = imageUrl;
            tmp.distance = calculateDistanceHistogram(histogramImage, histogramNeighbour);
		    cout<<tmp.name<<" "<<tmp.distance<<endl;
            for(int i = 0; i < nbNeighbour; i++){
                if(tmp.distance <= desImgs[i].distance){
                    for(int j = nbNeighbour - 1; j > i; j--){
                        desImgs[j].name = desImgs[j - 1].name;
                        desImgs[j].distance = desImgs[j - 1].distance;
                    }
                    desImgs[i].name = tmp.name;
                    desImgs[i].distance = tmp.distance;
                    break;
                }
            }

            delete histogramNeighbour;
        }
    }
}

void getHistogram(char *file, float *histogramArray){
    Mat image = imread(file, CV_LOAD_IMAGE_UNCHANGED);
    int totalPixel = 0;
    int maxIndex = scale * 3;

    for (int i = 0; i < maxIndex; i++){
            histogramArray[i] = 0;
    }

    for (int i = 0; i < image.size().width; i++){
        for (int j = 0; j < image.size().height; j++){
            Vec3b pixel = image.at<Vec3b>(j, i);

            int r = (int)pixel.val[0]*scale / 256;
            int g = (int)pixel.val[1]*scale / 256;
            int b = (int)pixel.val[2]*scale / 256;

            histogramArray[r] += 1;
            histogramArray[g+scale] += 1;
            histogramArray[g+scale*2] += 1;
        }
    }

    totalPixel += image.size().width * image.size().height;
    for(int i = 0; i < maxIndex; i++){
        histogramArray[i] = histogramArray[i] / totalPixel;
    }
}

float calculateDistanceHistogram(float *his1, float *his2){
    float a = 0;
    float b = 0;
    int maxIndex = scale * 3;
    for(int i = 0; i < maxIndex; i++){
        a += abs(his1[i] - his2[i]);
        b += his1[i];
    }
    return a / b;
}

